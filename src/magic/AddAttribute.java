package magic;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import magic.MyLabel;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.helpers.collection.IteratorUtil;

import com.csvreader.CsvReader;

import magic.Main;

public class AddAttribute {
	public static ExecutorService pool = Executors.newCachedThreadPool();
	public final static Logger logger = Logger.getLogger("attribute");
	public static Map<String, Map<String, String>> values = new HashMap<String, Map<String, String>>();
	public static Map<String, String> md5 = new HashMap<String, String>();

	private static void setMap() {
		CsvReader r;
		try {
			r = new CsvReader("/Neo4j/code/20150712/" + "node.csv", ',',
					Charset.forName("utf8"));
			while (r.readRecord()) {
				String serial_number = r.get(0);
				String post_code = r.get(3);
				String sex = r.get(4);
				String work_ci = r.get(5);
				String home_lac = r.get(6);
				String birth_city = r.get(7);
				String home_ci = r.get(8);
				String work_lac = r.get(11);
				String age = r.get(12);

				Map<String, String> map = new HashMap<String, String>();
				map.put("post_code", post_code);
				map.put("sex", sex);
				map.put("work_ci", work_ci);
				map.put("home_lac", home_lac);
				map.put("birth_city", birth_city);
				map.put("home_ci", home_ci);
				map.put("work_lac", work_lac);
				map.put("age", age);

				values.put(serial_number, map);
			}

			System.out.println("Size: " + values.size());
			r.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void setMD5() {
		CsvReader r;
		try {
			r = new CsvReader("/Neo4j/code/" + "MD5.csv", ',',
					Charset.forName("utf8"));
			while (r.readRecord()) {
				String serial_number = r.get(0);
				String MD5 = r.get(1);
				md5.put(MD5, serial_number);
			}

			System.out.println("Size: " + md5.size());
			r.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		setMap();
		setMD5();
		Main.DB_PATH = args[0];

		logger.setLevel(Level.INFO);

		try {
			Main.db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(
					Main.DB_PATH).newGraphDatabase();
			logger.info("Start database at: " + args[0]);
			Long start = System.currentTimeMillis();
			
			try (Transaction transaction = Main.db.beginTx();) {
				for (Node userNode : IteratorUtil.asSet(Main.db
						.findNodes(MyLabel.USER))) {
					String MD5 = userNode.getProperty(
							Constant.SERIAL_NUMBER).toString();
					userNode.setProperty("MD5", MD5);
					userNode.setProperty(Constant.SERIAL_NUMBER, md5.get(MD5));
				}
				transaction.success();
			}
			
			try (Transaction transaction = Main.db.beginTx();) {
				int count1 = 0;
				int count2 = 0;
				for (Node userNode : IteratorUtil.asSet(Main.db
						.findNodes(MyLabel.USER))) {
					String serial_number = userNode.getProperty(
							Constant.SERIAL_NUMBER).toString();
					if (values.containsKey(serial_number)) {
						Map<String, String> map = values.get(serial_number);
						for (Entry<String, String> entry : map.entrySet()) {
							userNode.setProperty(entry.getKey(),
									entry.getValue());
						}
						count2++;
					} else {
						count1++;
					}
				}
				transaction.success();
				System.out.println(count2);
				System.out.println(count1);
			}

			logger.info("Total time: " + (System.currentTimeMillis() - start)
					/ 1000 + "s.");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}

	}

}
