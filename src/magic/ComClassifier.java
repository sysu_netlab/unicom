package magic;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

public class ComClassifier {

	// private static double threshold = 0.5;
	// private static double alpha = 0.5;
	// private static double beta = 0.5;
	// private static double gamma = 0.8;

	public static void classifier() {

		Main.logger.info("Begin classification");

		try (Transaction transaction = Main.db.beginTx();) {

			ResourceIterator<Node> comIter = Main.db.findNodes(MyLabel.COMMUNITY);

			// do classification for every community
			while (comIter.hasNext()) {
				Node community = comIter.next();
				long[] best_ids = (long[]) community.getProperty(Constant.BEST_IDs);

				Set<Node> members = new HashSet<Node>();

				for (Relationship rela : community.getRelationships(MyRelationship.BELONG)) {
					Node otherNode = rela.getOtherNode(community);

					if (otherNode.hasLabel(MyLabel.USER)) {
						members.add(otherNode);
					}
				}

				// use best_ids to judge the classification of community
				classJudge(community, members, best_ids);
			}

			// judge the class of community
			transaction.success();
		}

		Main.logger.info("End classification");

		return;
	}

	// judge the class of community
	private static void classJudge(Node community, Set<Node> members, long[] best_ids) {

		Map<String, Integer> group_counter = new HashMap<String, Integer>();
		Map<String, Integer> loc_counter = new HashMap<String, Integer>();
		Map<String, Integer> birth_counter = new HashMap<String, Integer>();

		// max quantity
		int group_max = 0;
		int loc_max = 0;
		int birth_max = 0;

		// possibility
		ArrayList<Double> work_possibility = new ArrayList<Double>();
		ArrayList<Double> friend_possibility = new ArrayList<Double>();

		try (Transaction transaction = Main.db.beginTx();) {

			for (long id : best_ids) {
				Set<Node> centeral_node_nei = getCentralNei(id, members);

				for (Node mem : centeral_node_nei) {

					// ignore the user without enough information
					if (mem.getProperty(Constant.GROUP_ID).toString().equalsIgnoreCase(Constant.UNKNOWN)
							&& mem.getProperty(Constant.WORK_LAC).toString().equalsIgnoreCase(Constant.UNKNOWN)
							&& mem.getProperty(Constant.BIRTH_CITY).toString().equalsIgnoreCase(Constant.UNKNOWN)) {

						continue;
					}

					// location of base station
					String group = mem.getProperty(Constant.GROUP_ID).toString();
					String loc = mem.getProperty(Constant.WORK_LAC).toString();
					String birth = mem.getProperty(Constant.BIRTH_CITY).toString();

					// count group information
					if (!group.equalsIgnoreCase(Constant.UNKNOWN)) {
						if (group_counter.containsKey(group)) {
							Integer group_num = group_counter.get(group);

							group_counter.put(group, ++group_num);
						} else {
							group_counter.put(group, 1);
						}
					}

					// count birth_id information
					if (!birth.equalsIgnoreCase(Constant.UNKNOWN)) {
						if (birth_counter.containsKey(birth)) {
							Integer birth_num = birth_counter.get(birth);

							birth_counter.put(birth, ++birth_num);
						} else {
							birth_counter.put(birth, 1);
						}
					}
					// count location information
					if (!loc.equalsIgnoreCase("UNKNOWN")) {
						if (loc_counter.containsKey(loc)) {
							Integer loc_num = loc_counter.get(loc);

							loc_counter.put(loc, ++loc_num);
						} else {
							loc_counter.put(loc, 1);
						}
					}
				}

				// find the maximum
				group_max = maximum(group_counter);
				loc_max = maximum(loc_counter);
				birth_max = maximum(birth_counter);

				work_possibility.add(
						(double) group_max / centeral_node_nei.size() >= (double) loc_max / centeral_node_nei.size()
								? (double) group_max / centeral_node_nei.size()
								: (double) loc_max / centeral_node_nei.size());
				friend_possibility.add((double) birth_max / centeral_node_nei.size());

				// reset the counter
				group_counter.clear();
				loc_counter.clear();
				birth_counter.clear();
			}

			// add label
			addCommunityLabel(community, work_possibility, friend_possibility);

			transaction.success();

		}

	}

	// find all the neighbors of central node in this community
	private static Set<Node> getCentralNei(long centeral_id, Set<Node> members) {
		Node central_node = Main.db.getNodeById(centeral_id);

		Set<Node> central_node_nei = new HashSet<Node>();

		for (Relationship rela : central_node.getRelationships()) {
			Node nei = rela.getOtherNode(central_node);

			if (members.contains(nei))
				central_node_nei.add(nei);
		}

		return central_node_nei;
	}

	// calculate the max same attribution
	private static int maximum(Map<String, Integer> map) {

		int max = 0;

		for (Integer num : map.values()) {
			if (num > max) {
				max = num;
			}
		}

		return max;
	}

	// add label to community
	private static void addCommunityLabel(Node community, ArrayList<Double> work_possibility,
			ArrayList<Double> friend_possibility) {

		// set up format
		DecimalFormat dcmFmt = new DecimalFormat("0.00");

		double workP = 0.0;
		double friendP = 0.0;

		for (Double wp : work_possibility)
			workP += wp;
		workP /= work_possibility.size();

		for (Double fp : friend_possibility)
			friendP += fp;
		friendP /= friend_possibility.size();

		try (Transaction transaction = Main.db.beginTx();) {

			community.setProperty(Constant.WORK_COMMUNITY_POSSIBILITY, dcmFmt.format(workP));
			community.setProperty(Constant.FRIEND_COMMUNITY_POSSIBILITY, dcmFmt.format(friendP));

			// add label
			if (workP == 0.0 && friendP == 0.0) {
				community.addLabel(MyLabel.UNKNOWN);
			}

			if (workP > friendP) {
				community.addLabel(MyLabel.WORK);
			} else {
				community.addLabel(MyLabel.FRIEND);
			}

			transaction.success();

		}
	}
}
