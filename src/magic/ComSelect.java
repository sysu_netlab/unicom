//package magic;
//
//import java.util.ArrayList;
//import java.util.Comparator;
//import java.util.HashSet;
//import java.util.Set;
//import java.util.TreeSet;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//import org.neo4j.graphdb.Node;
//import org.neo4j.graphdb.Relationship;
//import org.neo4j.graphdb.Transaction;
//import org.neo4j.tooling.GlobalGraphOperations;
//
//public class ComSelect {
//
//	private final static int recomThreshold = 2;
//	private final static double similarThreshold = 0.3;
//
//	public static void selection() {
//
//		int counter = 0;
//
//		// store recommendation results
//		Set<Node> recommendedCom = new HashSet<Node>();
//
//		try (Transaction transaction = Main.db.beginTx()) {
//
//			Main.logger.info("Begin recommendation... \n");
//
//			// select for each user
//			for (Node node : GlobalGraphOperations.at(Main.db).getAllNodes()) {
//
//				if (!node.hasLabel(MyLabel.USER)) {
//					continue;
//				}
//
//				if (++counter % 100 == 0) {
//					Main.logger.info(counter + " nodes processed \n");
//				}
//
//				ArrayList<Node> comOfSinglePerson = new ArrayList<Node>();
//
//				// find all communities of single person
//				for (Relationship rela : node
//						.getRelationships(MyRelationship.BELONG)) {
//
//					Node comNode = rela.getOtherNode(node);
//
//					if (comNode.hasLabel(MyLabel.COMMUNITY)) {
//						comOfSinglePerson.add(comNode);
//					}
//				}
//
//				recommend(comOfSinglePerson, recommendedCom);
//			}
//
//			Main.logger.info("Output to Neo... \n");
//
//			// write recommendation to neo4j
//			outputToNeo(recommendedCom);
//
//			transaction.success();
//		}
//
//		Main.logger.info("End recommendation \n");
//	}
//
//	public static void outputToNeo(Set<Node> recommendedCom) {
//
//		try (Transaction transaction = Main.db.beginTx();) {
//
//			for (Node comNode : recommendedCom) {
//				comNode.addLabel(MyLabel.RECOMMEND);
//			}
//
//			transaction.success();
//		}
//	}
//
//	public static void recommend(ArrayList<Node> comOfSinglePerson,
//			Set<Node> recommendedCom) {
//
//		// just store the community node
//		ArrayList<TreeSet<Node>> allBuckets = new ArrayList<TreeSet<Node>>();
//
//		Set<Integer> usedCom = new HashSet<Integer>();
//
//		// clustering
//		for (int i = 0; i < comOfSinglePerson.size(); i++) {
//
//			// sorted set
//			// n1,n2 is the community node(NOT member node)
//			TreeSet<Node> bucket = new TreeSet<Node>(new Comparator<Node>() {
//				public int compare(Node n1, Node n2) {
//					if (n1.equals(n2)) {
//						return 0;
//					} else if (isBetter(n1, n2)) {
//						return -1;
//					} else {
//						return 1;
//					}
//				}
//			});
//
//			if (usedCom.contains(i)) {
//				continue;
//			}
//
//			bucket.add(comOfSinglePerson.get(i));
//			usedCom.add(i);
//
//			for (int j = i + 1; j < comOfSinglePerson.size(); j++) {
//
//				if (usedCom.contains(j)) {
//					continue;
//				}
//
//				if (similarity(comOfSinglePerson.get(i),
//						comOfSinglePerson.get(j)) >= similarThreshold) {
//					bucket.add(comOfSinglePerson.get(j));
//
//					usedCom.add(j);
//				}
//			}
//
//			allBuckets.add(bucket);
//		}
//
//		for (TreeSet<Node> singleBucket : allBuckets) {
//			int counter = 0;
//
//			for (Node com2 : singleBucket) {
//				recommendedCom.add(com2);
//
//				if (++counter >= recomThreshold) {
//					break;
//				}
//			}
//		}
//	}
//
//	public static double similarity(Node comNode1, Node comNode2) {
//
//		double similarity = 0;
//		int numOfSame = 0;
//
//		long[] bestId1 = null;
//		long[] bestId2 = null;
//
//		/*
//		 * Set<Node> com1 = new HashSet<Node>(); Set<Node> com2 = new
//		 * HashSet<Node>();
//		 */
//		/*
//		 * try (Transaction transaction = Main.db.beginTx();) {
//		 * 
//		 * //get all members of two communities for (Relationship rela1 :
//		 * comNode1 .getRelationships(MyRelationship.BELONG)) {
//		 * com1.add(rela1.getOtherNode(comNode1)); }
//		 * 
//		 * for (Relationship rela2 : comNode2
//		 * .getRelationships(MyRelationship.BELONG)) {
//		 * com2.add(rela2.getOtherNode(comNode2)); }
//		 * 
//		 * for (Node node : com1) { if (com2.contains(node)) { numOfSame++; } }
//		 * 
//		 * transaction.success(); }
//		 */
//
//		bestId1 = (long[]) comNode1.getProperty(Constant.BEST_IDs);
//		bestId2 = (long[]) comNode2.getProperty(Constant.BEST_IDs);
//
//		for (int i = 0; i < bestId1.length; i++) {
//			for (int j = 0; j < bestId2.length; j++) {
//				if (bestId1[i] == bestId2[j]) {
//					numOfSame++;
//
//					break;
//				}
//			}
//		}
//
//		/*
//		 * similarity = numOfSame / (Math.sqrt(bestId1.length) *
//		 * Math.sqrt(bestId2.length));
//		 */
//
//		similarity = (numOfSame / bestId1.length > numOfSame / bestId2.length) ? numOfSame
//				/ bestId1.length
//				: numOfSame / bestId2.length;
//
//		/*
//		 * similarity = numOfSame / (Math.sqrt(com1.size()) *
//		 * Math.sqrt(com2.size()));
//		 */
//
//		return similarity;
//	}
//
//	public static boolean isBetter(Node comNode1, Node comNode2) {
//
//		boolean result = false;
//
//		double conductance1 = calConductance(comNode1);
//		double conductance2 = calConductance(comNode2);
//
//		result = conductance1 >= conductance2;
//
//		return result;
//	}
//
//	public static double calConductance(Node comNode) {
//
//		double conductance = 0;
//
//		double innerWeight = 0;
//		double outterWeight = 0;
//
//		Set<Relationship> usedEdge = new HashSet<Relationship>();
//
//		Set<Node> community = new HashSet<Node>();
//
//		try (Transaction transaction = Main.db.beginTx();) {
//
//			for (Relationship rela : comNode
//					.getRelationships(MyRelationship.BELONG)) {
//				Node comMember = rela.getOtherNode(comNode);
//
//				community.add(comMember);
//			}
//
//			for (Node node : community) {
//				for (Relationship rela : node
//						.getRelationships(MyRelationship.CALLING)) {
//					if (!usedEdge.contains(rela)) {
//						if (community.contains(rela.getOtherNode(node))) {
//							innerWeight += Double.parseDouble(rela.getProperty(
//									Constant.WEIGHT).toString());
//						} else {
//							outterWeight += Double.parseDouble(rela
//									.getProperty(Constant.WEIGHT).toString());
//						}
//
//						usedEdge.add(rela);
//					}
//				}
//			}
//
//			transaction.success();
//		}
//
//		conductance = innerWeight / (outterWeight + innerWeight);
//
//		return conductance;
//	}
//}
