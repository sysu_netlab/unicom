package magic;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import com.csvreader.CsvWriter;

public class Community {
	static {
		try {
			String dir = Community.class.getResource("").getPath();
			dir = dir.substring(0, dir.indexOf("bin"));
			File file = new File(dir + "output/");
			if(!file.exists()){
				file.mkdirs();
			}
			nodeWriter = new CsvWriter(new FileWriter(dir + "output/user.csv",
					false), ',');
			edgeWriter = new CsvWriter(new FileWriter(dir + "output/edge.csv",
					false), ',');
			communityWriter = new CsvWriter(new FileWriter(dir
					+ "output/community.csv", false), ',');
		} catch (Exception e) {
			Main.logger.info(e.toString());
		}
	}
	public static CsvWriter nodeWriter;
	public static CsvWriter edgeWriter;
	public static CsvWriter communityWriter;

	private Set<Node> nodes;
	private Set<Relationship> relationships;

	private String community_id;
	private String community_name;
	private String post_code;

	private Integer size;

	private Long node_id;
	private long[] best_ids;
	private Map<Long, Item> centrality;

	public Community(String community_id, Set<Node> nodes) {
		this.community_id = community_id;
		this.community_name = Constant.COMMUNITY_PREFIX + "_" + community_id;
		this.post_code = "020";

		this.nodes = nodes;

		this.relationships = new HashSet<Relationship>();
		setRelationships();

		this.centrality = new HashMap<Long, Item>();
		initialCentrality();
		calCentrality();

		this.size = this.nodes.size();
		setCenter();
	}

	public Community(Node communityNode) {
		try (Transaction transaction = Main.db.beginTx();) {
			this.node_id = communityNode.getId();
			this.community_id = communityNode
					.getProperty(Constant.COMMUNITY_ID).toString();
			this.community_name = communityNode.getProperty(
					Constant.COMMUNITY_NAME).toString();
			this.best_ids = (long[]) communityNode
					.getProperty(Constant.BEST_IDs);
			this.size = Integer.parseInt(communityNode.getProperty(
					Constant.COMMUNITY_SIZE).toString());
			this.node_id = communityNode.getId();
			this.post_code = communityNode.getProperty(
					Constant.COMMUNITY_DOMAIN).toString();

			this.nodes = new HashSet<Node>();
			this.centrality = new HashMap<Long, Item>();
			for (Relationship rela : communityNode
					.getRelationships(MyRelationship.BELONG)) {
				Node membership = rela.getOtherNode(communityNode);
				this.nodes.add(membership);
				Item item = new Item();
				item.centrality1 = Double.parseDouble(rela.getProperty(
						Constant.CENTRALITY1).toString());
				item.centrality2 = Double.parseDouble(rela.getProperty(
						Constant.CENTRALITY2).toString());
				this.centrality.put(membership.getId(), item);
			}

			this.relationships = new HashSet<Relationship>();
			setRelationships();
			Main.logger.info(this.community_name + " has " + this.nodes.size()
					+ " nodes.");
			transaction.success();
		}
	}

	private void setRelationships() {
		for (Node node : this.nodes) {
			for (Relationship relationship : node
					.getRelationships(MyRelationship.CALLING)) {
				if (this.nodes.contains(relationship.getOtherNode(node))) {
					this.relationships.add(relationship);
				}
			}
		}
	}

	private void initialCentrality() {
		for (Node node : this.nodes) {
			Item item = new Item();
			item.centrality1 = 0.0;
			item.centrality2 = 0.0;
			this.centrality.put(node.getId(), item);
		}
	}

	private void calCentrality() {
		try (Transaction transaction = Main.db.beginTx();) {
			for (Relationship relationship : this.relationships) {
				Node startNode = relationship.getStartNode();
				Node endNode = relationship.getEndNode();
				this.centrality.get(startNode.getId()).centrality1 += 1.0;
				this.centrality.get(endNode.getId()).centrality1 += 1.0;
				this.centrality.get(startNode.getId()).centrality2 += Double
						.parseDouble(relationship.getProperty(Constant.WEIGHT)
								.toString());
				this.centrality.get(endNode.getId()).centrality2 += Double
						.parseDouble(relationship.getProperty(Constant.WEIGHT)
								.toString());
			}
			transaction.success();
		}
	}

	private void setCenter() {
		List<Long> best_ids = new ArrayList<Long>();
		Double value = 0.0;
		for (Entry<Long, Item> entry : this.centrality.entrySet()) {
			if (entry.getValue().centrality1 > value) {
				value = entry.getValue().centrality1;
			}
		}

		for (Entry<Long, Item> entry : this.centrality.entrySet()) {
			if ((value - entry.getValue().centrality1 > 0.001)) {
				continue;
			}
			best_ids.add(entry.getKey());
		}

		this.best_ids = new long[1];
		if (best_ids.size() != 1) {
			Long best_id = 0L;
			Double weight = 0.0;
			for (Long id : best_ids) {
				if (weight < this.centrality.get(id).centrality2) {
					best_id = id;
				}
			}
			this.best_ids[0] = best_id;
		} else {
			this.best_ids[0] = best_ids.get(0);
		}

	}

	public void buildCommunity() {
		Main.logger.info("Building community: " + this.community_name);
		try (Transaction transaction = Main.db.beginTx();) {
			Node communityNode = Main.db.createNode(MyLabel.COMMUNITY);
			communityNode.addLabel(MyLabel.NODE);
			this.node_id = communityNode.getId();

			communityNode.setProperty(Constant.BEST_IDs, this.best_ids);
			communityNode
					.setProperty(Constant.COMMUNITY_DOMAIN, this.post_code);
			communityNode.setProperty(Constant.COMMUNITY_SIZE, this.size);
			communityNode.setProperty(Constant.COMMUNITY_NAME,
					this.community_name);
			communityNode.setProperty(Constant.COMMUNITY_ID, this.community_id);

			for (Entry<Long, Item> entry : this.centrality.entrySet()) {
				Relationship rela = communityNode.createRelationshipTo(
						Main.db.getNodeById(entry.getKey()),
						MyRelationship.BELONG);
				rela.setProperty(Constant.CENTRALITY1,
						entry.getValue().centrality1);
				rela.setProperty(Constant.CENTRALITY2,
						entry.getValue().centrality2);
			}

			for (long best_id : this.best_ids) {
				Relationship rela = communityNode.createRelationshipTo(
						Main.db.getNodeById(best_id), MyRelationship.IMPORTANT);
				rela.setProperty(Constant.CENTRALITY1,
						this.centrality.get(best_id).centrality1);
				rela.setProperty(Constant.CENTRALITY2,
						this.centrality.get(best_id).centrality2);
			}

			transaction.success();
		}
	}

	public void outputInfo() {
		List<String[]> communityInfo = new ArrayList<String[]>();
		List<String[]> nodeInfo = new ArrayList<String[]>();
		List<String[]> relationshipInfo = new ArrayList<String[]>();

		try (Transaction transaction = Main.db.beginTx();) {
			for (int i = 0; i < best_ids.length; i++) {
				Node bestNode = Main.db.getNodeById(this.best_ids[i]);
				String[] row = {
						post_code,
						bestNode.getProperty(Constant.USER_ID).toString(),
						bestNode.getProperty(Constant.SERIAL_NUMBER).toString(),
						community_id, community_name, size.toString() };
				communityInfo.add(row);
			}

			for (Node node : this.nodes) {
				String[] row = {
						community_id.toString(),
						node.getProperty(Constant.SERIAL_NUMBER).toString(),
						String.format("%.2f",
								this.centrality.get(node.getId()).centrality1) };
				nodeInfo.add(row);
			}

			for (Relationship relationship : this.relationships) {
				String[] row = {
						community_id.toString(),
						relationship.getStartNode()
								.getProperty(Constant.SERIAL_NUMBER).toString(),
						relationship.getEndNode()
								.getProperty(Constant.SERIAL_NUMBER).toString(),
						String.format(
								"%.2f",
								Double.parseDouble(relationship.getProperty(
										Constant.WEIGHT).toString())),
						String.valueOf("0") };

				relationshipInfo.add(row);
			}

			this.output(Community.communityWriter, communityInfo);
			this.output(Community.nodeWriter, nodeInfo);
			this.output(Community.edgeWriter, relationshipInfo);
			transaction.success();
		}
	}

	private void output(CsvWriter writer, List<String[]> contents) {
		try {
			for (String[] row : contents) {
				writer.writeRecord(row);
			}
			writer.flush();
		} catch (IOException e) {
			Main.logger.info(e.toString());
		} finally {

		}

	}
}
