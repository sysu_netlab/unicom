package magic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

public class CommunityDedectionEdge {
	public static double callWeightThreshold = 10.0;
	public static int communityCount = 0;
	public static int redivideCommunitySizeThreshold = 20;
	public static int continuousTwoPeopleCommunityThreshold = 50;
	public static int continuousTwoPeopleCommunityCount = 0;
	public static final double filterRatio = 0.5;

	public static void detection(Set<Node> communityIn, int depth) {
		Map<Relationship, Double> allEdges = getAllRelaInCommunity(communityIn);
		Map<Relationship, Double> remainEdges = getAllRelaInCommunity(communityIn);

		Set<Relationship> allEdgesSet = allEdges.keySet();

		try (Transaction transaction = Main.db.beginTx();) {

			Main.logger.info("Begin detection. Depth: " + depth);

			while (remainEdges.size() > 0) {
				if (depth == 0
						&& continuousTwoPeopleCommunityCount > continuousTwoPeopleCommunityThreshold) {
					Main.logger
							.info("Continuous two people community exceeds limit! Early stop!");
					break;
				}

				Set<Node> community = new HashSet<Node>();
				Set<Relationship> innerEdges = new HashSet<Relationship>();
				double conductanceOfCommunity = 0.0;

				Map.Entry<Relationship, Double> top = remainEdges.entrySet()
						.iterator().next();

				Relationship initialEdge = top.getKey();
				innerEdges.add(initialEdge);

				community.add(initialEdge.getStartNode());
				community.add(initialEdge.getEndNode());

				conductanceOfCommunity = conductance(community, allEdgesSet);
				Set<Node> neighbors = new HashSet<Node>();

				Node startNode = initialEdge.getStartNode();

				for (Relationship rela : startNode
						.getRelationships(MyRelationship.CALLING)) {
					if (remainEdges.containsKey(rela)) {
						Node otherNode = rela.getOtherNode(startNode);
						if (!community.contains(otherNode)) {
							neighbors.add(otherNode);
						}
					}
				}

				Node endNode = initialEdge.getEndNode();

				for (Relationship rela : endNode
						.getRelationships(MyRelationship.CALLING)) {
					if (remainEdges.containsKey(rela)) {
						Node otherNode = rela.getOtherNode(endNode);
						if (!community.contains(otherNode)) {
							neighbors.add(otherNode);
						}
					}
				}

				while (!neighbors.isEmpty()) {
					double current_max_belonging_degree = 0.0;
					Node current_max_belonging_degree_node = null;

					for (Node node : neighbors) {
						double belonging_degree = belonging_degree(node,
								community, allEdgesSet);
						if (belonging_degree > current_max_belonging_degree) {
							current_max_belonging_degree = belonging_degree;
							current_max_belonging_degree_node = node;
						}
					}

					double threshold = (1.0 - conductanceOfCommunity)
							/ (2.0 - conductanceOfCommunity);

					if (current_max_belonging_degree > threshold) {

						if (current_max_belonging_degree == 1.0) {
							double weightCount = 0;
							for (Relationship rela : current_max_belonging_degree_node
									.getRelationships(MyRelationship.CALLING)) {
								weightCount += Double.parseDouble(rela
										.getProperty(Constant.WEIGHT)
										.toString());
							}
							if (weightCount < callWeightThreshold) {
								neighbors
										.remove(current_max_belonging_degree_node);
								continue;
							}
						}

						community.add(current_max_belonging_degree_node);

						conductanceOfCommunity = conductance(community,
								allEdgesSet);
						neighbors.remove(current_max_belonging_degree_node);

						for (Relationship rela : current_max_belonging_degree_node
								.getRelationships(MyRelationship.CALLING)) {
							if (remainEdges.containsKey(rela)) {
								Node otherNode = rela
										.getOtherNode(current_max_belonging_degree_node);
								if (!community.contains(otherNode)) {
									neighbors.add(otherNode);
								} else {
									innerEdges.add(rela);
								}
							}
						}
					} else {
						break;
					}
				}

				for (Relationship rela : innerEdges) {
					remainEdges.remove(rela);
				}

				if (depth == 0) {
					if (community.size() > redivideCommunitySizeThreshold) {
						detection(community, depth + 1);
					} else {
						if (community.size() == 2) {
							continuousTwoPeopleCommunityCount++;
						} else {
							continuousTwoPeopleCommunityCount = 0;
						}

						if (community.size() >= 4) {
							Set<Node> newCommunity = filter(community);
							if (newCommunity.size() >= 4) {
								communityCount++;
								Community communityObject = new Community(
										String.valueOf(communityCount),
										newCommunity);
								communityObject.buildCommunity();
							}
						}

					}
				} else {
					if (community.size() > redivideCommunitySizeThreshold
							&& community.size() < communityIn.size()) {
						detection(community, depth + 1);
					} else {
						if (community.size() >= 4) {
							Set<Node> newCommunity = filter(community);
							if (newCommunity.size() >= 4) {
								communityCount++;
								Community communityObject = new Community(
										String.valueOf(communityCount),
										newCommunity);
								communityObject.buildCommunity();
							}
						}
					}
				}
			}
			transaction.success();
		}

		Main.logger.info("End detection. Depth: " + depth);
	}

	public static Set<Node> filter(Set<Node> community) {
		Map<Node, Integer> nodeCountMap = new LinkedHashMap<Node, Integer>();
		Set<Node> allNodeSet = community;
		Set<Node> filteredNodeSet = new HashSet<Node>();
		Set<Relationship> allEdgeSet = new HashSet<Relationship>();
		double threshold = 0;

		try (Transaction transaction = Main.db.beginTx();) {
			for (Node node : allNodeSet) {
				for (Relationship relationship : node
						.getRelationships(MyRelationship.CALLING)) {
					if (allNodeSet.contains(relationship.getOtherNode(node))) {
						allEdgeSet.add(relationship);

						if (nodeCountMap.containsKey(node)) {
							nodeCountMap.put(node, nodeCountMap.get(node) + 1);
						} else {
							nodeCountMap.put(node, 1);
						}
					}
				}
			}

			List<Double> weightList = new ArrayList<Double>();
			for (Relationship relationship : allEdgeSet) {
				weightList.add(Double.parseDouble(relationship.getProperty(
						Constant.WEIGHT).toString()));
			}
			Collections.sort(weightList);
			double medium = 0;
			if (weightList.size() / 2 == 0) {
				medium = (weightList.get(weightList.size() / 2 - 1) + weightList
						.get(weightList.size() / 2)) / 2;
			} else {
				medium = weightList.get(weightList.size() / 2);
			}

			threshold = medium * filterRatio;

			for (Relationship relationship : allEdgeSet) {
				if (Double.parseDouble(relationship
						.getProperty(Constant.WEIGHT).toString()) < threshold) {
					Node startNode = relationship.getStartNode();
					nodeCountMap
							.put(startNode, nodeCountMap.get(startNode) - 1);

					Node endNode = relationship.getEndNode();
					nodeCountMap.put(endNode, nodeCountMap.get(endNode) - 1);
				}
			}

			Iterator<Map.Entry<Node, Integer>> iter = nodeCountMap.entrySet()
					.iterator();
			while (iter.hasNext()) {
				Map.Entry<Node, Integer> tempEntry = iter.next();
				int count = tempEntry.getValue();
				if (count > 0) {
					filteredNodeSet.add(tempEntry.getKey());
				}
			}

			transaction.success();
		}
		return filteredNodeSet;
	}

	public static double conductance(Set<Node> community,
			Set<Relationship> allEdgesSet) {
		double conductance = 0.0;
		Set<Relationship> cutEdgeSet = new HashSet<Relationship>();
		Set<Relationship> innerEdgeSet = new HashSet<Relationship>();
		double cutEdgeWeightSum = 0.0;
		double innerEdgeWeightSum = 0.0;

		try (Transaction transaction = Main.db.beginTx();) {
			for (Node node : community) {
				for (Relationship rela : node
						.getRelationships(MyRelationship.CALLING)) {
					if (allEdgesSet.contains(rela)) {
						if (community.contains(rela.getOtherNode(node))) {
							if (!innerEdgeSet.contains(rela)) {
								innerEdgeSet.add(rela);
								innerEdgeWeightSum += (Double.parseDouble(rela
										.getProperty(Constant.WEIGHT)
										.toString()));
							}
						} else {
							if (!cutEdgeSet.contains(rela)) {
								cutEdgeSet.add(rela);
								cutEdgeWeightSum += (Double.parseDouble(rela
										.getProperty(Constant.WEIGHT)
										.toString()));
							}
						}
					}
				}
			}

			transaction.success();
		}
		conductance = cutEdgeWeightSum
				/ (cutEdgeWeightSum + innerEdgeWeightSum);
		return conductance;
	}

	public static double belonging_degree(Node node, Set<Node> community,
			Set<Relationship> allEdgesSet) {
		double result = 0.0;
		double w = 0.0;
		double ku = 0.0;

		try (Transaction transaction = Main.db.beginTx();) {
			for (Relationship rela : node
					.getRelationships(MyRelationship.CALLING)) {
				if (allEdgesSet.contains(rela)) {
					ku += Double.parseDouble(rela.getProperty(Constant.WEIGHT)
							.toString());
					Node nei = rela.getOtherNode(node);

					if (community.contains(nei)) {
						w += Double.parseDouble(rela.getProperty(
								Constant.WEIGHT).toString());
					}
				}
			}

			transaction.success();
		}

		result = w / ku;

		return result;
	}

	public static Map<Relationship, Double> getAllRelaInCommunity(
			Set<Node> community) {
		Map<Relationship, Double> relaMap = new HashMap<Relationship, Double>();

		try (Transaction transaction = Main.db.beginTx();) {
			for (Node node : community) {
				for (Relationship rela : node
						.getRelationships(MyRelationship.CALLING)) {
					if (community.contains(rela.getOtherNode(node))) {
						double weight = 0.0;
						weight = Double.parseDouble(rela.getProperty(
								Constant.WEIGHT).toString());
						relaMap.put(rela, weight);
					}
				}
			}

			relaMap = sortMapByValue(relaMap);
			transaction.success();
		}
		return relaMap;
	}

	public static Map<Relationship, Double> sortMapByValue(
			Map<Relationship, Double> oriMap) {
		Map<Relationship, Double> sortedMap = new LinkedHashMap<Relationship, Double>();

		if (oriMap != null && !oriMap.isEmpty()) {
			List<Map.Entry<Relationship, Double>> entryList = new ArrayList<Map.Entry<Relationship, Double>>(
					oriMap.entrySet());
			Collections.sort(entryList,
					new Comparator<Map.Entry<Relationship, Double>>() {
						public int compare(Entry<Relationship, Double> entry1,
								Entry<Relationship, Double> entry2) {
							double value1 = 0;
							double value2 = 0;

							try {
								value1 = entry1.getValue();
								value2 = entry2.getValue();
							} catch (NumberFormatException e) {
								value1 = 0.0;
								value2 = 0.0;
							}

							if (value2 > value1) {
								return 1;
							} else if (value2 < value1) {
								return -1;
							} else {
								return 0;
							}
						}
					});

			Iterator<Map.Entry<Relationship, Double>> iter = entryList
					.iterator();

			Map.Entry<Relationship, Double> tempEntry = null;

			while (iter.hasNext()) {
				tempEntry = iter.next();
				sortedMap.put(tempEntry.getKey(), tempEntry.getValue());
			}
		}

		return sortedMap;
	}
}
