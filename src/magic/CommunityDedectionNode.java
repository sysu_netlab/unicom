package magic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.helpers.collection.IteratorUtil;
import org.neo4j.tooling.GlobalGraphOperations;

public class CommunityDedectionNode {

	private final static Logger logger = Logger.getLogger("comDect");

	public static void detection() throws SecurityException, IOException {
		logger.setLevel(Level.INFO);

		// initial the map<node, weight>
		Map<Node, Double> nodes = initialization();

		// store the remaining nodes
		Set<Node> remainNodes = new HashSet<Node>();

		for (Node node : nodes.keySet()) {
			remainNodes.add(node);
		}

		Set<Map.Entry<Node, Double>> nodeSet = nodes.entrySet();
		Iterator<Map.Entry<Node, Double>> iter = nodeSet.iterator();

		logger.info("Begin detection\n");

		int counter = 0;

		try (Transaction transaction = Main.db.beginTx();) {
			// begin the algorithm
			while (iter.hasNext()) {

				// first stage
				Set<Node> community = new HashSet<Node>();

				Map.Entry<Node, Double> top = iter.next();

				// get the initial node
				Node initNode = top.getKey();

				if (!remainNodes.contains(initNode)) {
					continue;
				}

				community.add(initNode);

				Set<Node> tempCom = new HashSet<Node>();

				tempCom.add(initNode);

				for (Relationship rela : initNode
						.getRelationships(MyRelationship.CALLING)) {
					Node nei = rela.getOtherNode(initNode);

					if (calConduct(tempCom, nei)) {
						tempCom.add(nei);
					}
				}

				for (Node node : tempCom) {
					community.add(node);

					remainNodes.remove(node);
				}

				for (Node node : community) {
					for (Relationship rela : node
							.getRelationships(MyRelationship.CALLING)) {
						Node nei = rela.getOtherNode(node);

						if (!community.contains(nei)
								&& calConduct(tempCom, nei)) {
							tempCom.add(nei);
						}
					}
				}

				for (Node node : tempCom) {
					community.add(node);

					remainNodes.remove(node);
				}

				// add community
				counter++;
				Community communityObject = new Community("n_" + counter,
						community);
				communityObject.buildCommunity();

			}
			transaction.success();
		}
	}

	// calculate conduct
	public static boolean calConduct(Set<Node> community, Node target) {

		Set<Node> tempCom = new HashSet<Node>();

		for (Node node : community) {
			tempCom.add(node);
		}

		tempCom.add(target);

		double oriConduct = 0;
		double newConduct = 0;

		try (Transaction transaction = Main.db.beginTx();) {

			double innerOri = 0;
			double outterOri = 0;
			double innerNew = 0;
			double outterNew = 0;

			for (Node node : community) {
				for (Relationship rela : node
						.getRelationships(MyRelationship.CALLING)) {
					Node otherNode = rela.getOtherNode(node);

					if (community.contains(otherNode)) {
						innerOri += Double.parseDouble(rela.getProperty(
								Constant.WEIGHT).toString());
					} else {
						outterOri += Double.parseDouble(rela.getProperty(
								Constant.WEIGHT).toString());
					}
				}
			}

			for (Node node : tempCom) {
				for (Relationship rela : node
						.getRelationships(MyRelationship.CALLING)) {
					Node otherNode = rela.getOtherNode(node);

					if (tempCom.contains(otherNode)) {
						innerNew += Double.parseDouble(rela.getProperty(
								Constant.WEIGHT).toString());
					} else {
						outterNew += Double.parseDouble(rela.getProperty(
								Constant.WEIGHT).toString());
					}
				}
			}

			oriConduct = innerOri / (outterOri + innerOri);
			newConduct = innerNew / (outterNew + innerNew);
			transaction.success();
		}

		return newConduct > oriConduct;
	}

	// calculate node strength
	public static Map<Node, Double> initialization() {
		Map<Node, Double> nodes = new HashMap<Node, Double>();

		try (Transaction transaction = Main.db.beginTx();) {
			// load the whole db into memory
			for (Node node : GlobalGraphOperations.at(Main.db).getAllNodes()) {
				IteratorUtil.count(node.getRelationships());
			}

			logger.info("End loading database\n");

			// add nodes to map<node, weight>
			for (Node node : GlobalGraphOperations.at(Main.db).getAllNodes()) {

				if (!node.hasLabel(MyLabel.USER)) {
					continue;
				}

				double weight = 0.0;

				for (Relationship relationship : node
						.getRelationships(MyRelationship.CALLING)) {
					double temp_weight = Double.parseDouble(relationship
							.getProperty(Constant.WEIGHT).toString());

					weight += temp_weight;

				}

				nodes.put(node, new Double(weight));
			}

			transaction.success();

			// sort the map decreasingly
			nodes = sortMapByValue(nodes);
		}

		return nodes;
	}

	// sort map decreasingly
	public static Map<Node, Double> sortMapByValue(Map<Node, Double> oriMap) {
		Map<Node, Double> sortedMap = new LinkedHashMap<Node, Double>();

		if (oriMap != null && !oriMap.isEmpty()) {
			List<Map.Entry<Node, Double>> entryList = new ArrayList<Map.Entry<Node, Double>>(
					oriMap.entrySet());
			Collections.sort(entryList,
					new Comparator<Map.Entry<Node, Double>>() {
						public int compare(Entry<Node, Double> entry1,
								Entry<Node, Double> entry2) {
							double value1 = 0;
							double value2 = 0;

							try {
								value1 = entry1.getValue();
								value2 = entry2.getValue();
							} catch (NumberFormatException e) {
								value1 = 0.0;
								value2 = 0.0;
							}

							if (value2 > value1) {
								return 1;
							} else if (value2 < value1) {
								return -1;
							} else {
								return 0;
							}
						}
					});

			Iterator<Map.Entry<Node, Double>> iter = entryList.iterator();

			Map.Entry<Node, Double> tempEntry = null;

			while (iter.hasNext()) {
				tempEntry = iter.next();
				sortedMap.put(tempEntry.getKey(), tempEntry.getValue());
			}
		}

		return sortedMap;
	}
}
