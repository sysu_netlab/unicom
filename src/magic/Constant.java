package magic;

public class Constant {
	public static final String COMMUNITY_PREFIX = "community";

	/*
	 * Relationship Properties
	 */
	public static final String WEIGHT = "weight";
	public static final String DISTANCE = "distance";
	public static final String CENTRALITY1 = "centrality1";
	public static final String CENTRALITY2 = "centrality2";

	/*
	 * User Node Properties
	 */
	public static final String SERIAL_NUMBER = "serial_number";
	public static final String USER_ID = "user_id";
	public static final String REAL_NAME = "realName";
	public static final String DEPT = "dept";
	public static final String POSTCODE = "post_code";
	public static final String WORK_LAC = "work_lac";
	public static final String WORK_CI = "work_ci";
	public static final String HOME_LAC = "home_lac";
	public static final String HOME_CI = "home_ci";
	public static final String AGE = "age";
	public static final String SEX = "sex";
	public static final String CITY = "city";
	public static final String GROUP_ID = "group_id";
	public static final String PREFER_WORK_COMMUNITY = "prefer_work_community";
	public static final String PREFER_FRIEND_COMMUNITY = "prefer_friend_community";
	public static final String PREFER_UNKNOWN_COMMUNITY = "prefer_unknown_community";
	public static final String BIRTH_CITY = "birth_city";
	public static final String UNKNOWN = "UNKNOWN";
	
	
	/*
	 * Community Node Properties
	 */
	public static final String BEST_IDs = "best_ids";
	public static final String COMMUNITY_ID = "community_id";
	public static final String COMMUNITY_SIZE = "community_size";
	public static final String COMMUNITY_NAME = "community_name";
	public static final String COMMUNITY_DOMAIN = "community_domain";
	public static final String WORK_COMMUNITY_POSSIBILITY = "work_possibility";
	public static final String FRIEND_COMMUNITY_POSSIBILITY = "friend_possibility";

}
