package magic;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.helpers.collection.IteratorUtil;
import org.neo4j.tooling.GlobalGraphOperations;

public class Main {
	public static String DB_PATH;
	public static GraphDatabaseService db;
	public static ExecutorService pool = Executors.newCachedThreadPool();
	public final static Logger logger = Logger.getLogger("detection");

	private static void registerShutdownHook(final GraphDatabaseService graphDb) {
		logger.info("Add shudown hook");
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDb.shutdown();
			}
		});
	}

	public static void main(String[] args) {
		logger.setLevel(Level.INFO);

		DB_PATH = args[0];
		try {
			db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(DB_PATH)
					.newGraphDatabase();
			logger.info("Start database at: " + DB_PATH);
			Long start = System.currentTimeMillis();

			// Extract all the users in the database.
			Set<Node> allUserNodeSet = new HashSet<Node>();
			try (Transaction transaction = Main.db.beginTx();) {
				ResourceIterator<Node> allUserNodes = db
						.findNodes(MyLabel.USER);
				while (allUserNodes.hasNext()) {
					allUserNodeSet.add(allUserNodes.next());
				}
				transaction.success();
			}

			// Community detection use the dataset.
		//	CommunityDedectionEdge.detection(allUserNodeSet, 0);

			try (Transaction transaction = Main.db.beginTx();) {
				for (Node communityNode : IteratorUtil.asSet(Main.db
						.findNodes(MyLabel.FRIEND))) {
					communityNode.removeLabel(MyLabel.FRIEND);
				}
				for (Node communityNode : IteratorUtil.asSet(Main.db
						.findNodes(MyLabel.WORK))) {
					communityNode.removeLabel(MyLabel.WORK);
				}
				for (Node communityNode : IteratorUtil.asSet(Main.db
						.findNodes(MyLabel.UNKNOWN))) {
					communityNode.removeLabel(MyLabel.UNKNOWN);
				}
				transaction.success();
			}
			// Classify the communities.
			ComClassifier.classifier();

			// Clustering the community and recommend the TopN communities of
			// each nodes.
			// ComSelect.selection();

			// Try to output the community and user information in csv format.
			try (Transaction transaction = Main.db.beginTx();) {
				for (Node communityNode : IteratorUtil.asSet(Main.db
						.findNodes(MyLabel.COMMUNITY))) {
					Community community = new Community(communityNode);
					community.outputInfo();
				}
				transaction.success();
			}

			try (Transaction transaction = Main.db.beginTx();) {
				for (Relationship rela : GlobalGraphOperations.at(Main.db)
						.getAllRelationships()) {
					if (rela.isType(MyRelationship.RECOMMEND)) {
						rela.delete();
					}
				}

				for (Node userNode : IteratorUtil.asSet(Main.db
						.findNodes(MyLabel.USER))) {
					User user = new User(userNode);
					user.recommendCommunity(MyLabel.WORK);
					user.recommendCommunity(MyLabel.FRIEND);
					user.recommendCommunity(MyLabel.UNKNOWN);
				}
				transaction.success();
			}
			logger.info("Total time: " + (System.currentTimeMillis() - start)
					/ 1000 + "s.");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			registerShutdownHook(db);
		}
	}

}
