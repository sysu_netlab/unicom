package magic;

import org.neo4j.graphdb.Label;

public enum MyLabel implements Label {
	NODE, USER, COMMUNITY, RECOMMEND, WORK, FRIEND, UNKNOWN
}