package magic;

import org.neo4j.graphdb.RelationshipType;

public enum MyRelationship implements RelationshipType {
	CALLING, BELONG, IMPORTANT, RECOMMEND
}
