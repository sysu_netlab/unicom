package magic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.helpers.collection.IteratorUtil;

public class User {
	private Long node_id;
	private Node user_node;

	public User(Node user) {
		this.user_node = user;

	}

	public void recommendCommunity(Label communityLabel) {
		try (Transaction transaction = Main.db.beginTx();) {
			this.node_id = this.user_node.getId();

			Set<Node> community = new HashSet<Node>();
			Map<Node, Item> nodes = new HashMap<Node, Item>();
			for (Relationship rela : this.user_node
					.getRelationships(MyRelationship.IMPORTANT)) {
				if (!rela.getOtherNode(this.user_node).hasLabel(communityLabel)) {
					continue;
				}
				Item item = new Item();
				item.centrality1 = Double.parseDouble(rela.getProperty(
						Constant.CENTRALITY1).toString());
				item.centrality2 = Double.parseDouble(rela.getProperty(
						Constant.CENTRALITY2).toString());
				community.add(rela.getOtherNode(this.user_node));
				nodes.put(rela.getOtherNode(this.user_node), item);
			}

			List<Node> prefer_communities = sort(nodes);
			Map<Node, Item> nodes2 = new HashMap<Node, Item>();
			for (Relationship rela : this.user_node
					.getRelationships(MyRelationship.BELONG)) {
				if (!rela.getOtherNode(this.user_node).hasLabel(communityLabel)) {
					continue;
				}
				if (community.contains(rela.getOtherNode(this.user_node))) {
					continue;
				}
				Item item = new Item();
				item.centrality1 = Double.parseDouble(rela.getProperty(
						Constant.CENTRALITY1).toString());
				item.centrality2 = Double.parseDouble(rela.getProperty(
						Constant.CENTRALITY2).toString());
				nodes2.put(rela.getOtherNode(this.user_node), item);
			}
			prefer_communities.addAll(sort(nodes2));

			long[] prefer_communities_node_ids = new long[prefer_communities
					.size()];

			int index = 0;
			for (Node node : prefer_communities) {
				prefer_communities_node_ids[index] = node.getId();
				index++;
			}

			if (communityLabel.name() == MyLabel.WORK.name()) {
				this.user_node.setProperty(Constant.PREFER_WORK_COMMUNITY,
						prefer_communities_node_ids);
			} else if (communityLabel.name() == MyLabel.FRIEND.name()) {
				this.user_node.setProperty(Constant.PREFER_FRIEND_COMMUNITY,
						prefer_communities_node_ids);
			} else {
				this.user_node.setProperty(Constant.PREFER_UNKNOWN_COMMUNITY,
						prefer_communities_node_ids);
			}

			/*
			 * if (prefer_communities_node_ids.length == 0) {
			 * Main.logger.info("User " +
			 * this.user_node.getProperty(Constant.SERIAL_NUMBER) .toString() +
			 * " doesn't have any community."); } else {
			 * Main.logger.info("User " +
			 * this.user_node.getProperty(Constant.SERIAL_NUMBER) .toString() +
			 * "'s recommend community(s) has " +
			 * prefer_communities_node_ids.length + " recommend community(s)");
			 * 
			 * for (int i = 0; i < prefer_communities_node_ids.length && i < 5;
			 * i++) { Node communityNode = Main.db
			 * .getNodeById(prefer_communities_node_ids[i]); Relationship rela =
			 * communityNode.createRelationshipTo( this.user_node,
			 * MyRelationship.RECOMMEND); Item item = null; if
			 * (nodes.containsKey(communityNode)) { item =
			 * nodes.get(communityNode); } else { item =
			 * nodes2.get(communityNode); } if (item != null) {
			 * rela.setProperty(Constant.CENTRALITY1, item.centrality1);
			 * rela.setProperty(Constant.CENTRALITY2, item.centrality2); }
			 * 
			 * } }
			 */

			transaction.success();
		}
	}

	private List<Node> sort(Map<Node, Item> nodesMap) {
		List<Node> prefer_communities = new ArrayList<Node>(nodesMap.size());
		List<Map.Entry<Node, Item>> entryList = new ArrayList<Map.Entry<Node, Item>>(
				nodesMap.entrySet());
		Collections.sort(entryList, new Comparator<Map.Entry<Node, Item>>() {
			public int compare(Entry<Node, Item> entry1,
					Entry<Node, Item> entry2) {
				double value1 = 0;
				double value2 = 0;

				try {
					value1 = entry1.getValue().centrality1;
					value2 = entry2.getValue().centrality1;
				} catch (NumberFormatException e) {
					value1 = 0.0;
					value2 = 0.0;
				}

				if (value2 > value1) {
					return 1;
				} else if (value2 < value1) {
					return -1;
				} else {
					value1 = entry1.getValue().centrality2;
					value2 = entry2.getValue().centrality2;
					if (value2 > value1) {
						return 1;
					} else if (value2 < value1) {
						return -1;
					} else {
						return 0;
					}
				}
			}
		});

		for (Entry<Node, Item> entry : entryList) {
			prefer_communities.add(entry.getKey());
		}
		return prefer_communities;
	}
}
