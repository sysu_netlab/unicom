package test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import magic.MyLabel;
import magic.MyRelationship;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

public class Jesse_Test {

	public static String DB_PATH;
	public static GraphDatabaseService db;

	private final static Logger logger = Logger.getLogger("evalue");

	public static void startup() {
		try {
			db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(DB_PATH)
					.newGraphDatabase();

			logger.info("Start database \n");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			registerShutdownHook(db);
		}
	}

	private static void registerShutdownHook(final GraphDatabaseService graphDb) {
		System.out.println("Add shudown hook");
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDb.shutdown();
			}
		});
	}

	private static double getScore(double a, double b) {
		if (a == 0.0 || b == 0.0) {
			return 0.0;
		} else {
			return 2 * a * b / (a + b);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		DB_PATH = args[0];

		startup();

		int size = 4;

		Long[] nodeId = { 45786L, 19949L, 29642L, 38447L };
		String[] names = { "Wang Fei", "Xiao Fang", "Wen Yuan", "Wen Jie" };

		try (Transaction transaction = db.beginTx();) {

			for (int i = 0; i < size; i++) {

				Set<Node> communities = new HashSet<Node>();
				Node user = db.getNodeById(nodeId[i]);

				for (Relationship rela : user
						.getRelationships(MyRelationship.BELONG)) {

					Node com = rela.getOtherNode(user);

					if (com.hasLabel(MyLabel.COMMUNITY)) {
						communities.add(com);
					}
				}

				List<Set<Node>> communityList = new ArrayList<Set<Node>>();
				for (Node community : communities) {
					Set<Node> members = new HashSet<Node>();
					for (Relationship rela : community
							.getRelationships(MyRelationship.BELONG)) {
						members.add(rela.getOtherNode(community));
					}
					communityList.add(members);
				}

				double[][] matrix = new double[communityList.size()][communityList
						.size()];

				for (int j = 0; j < communityList.size(); j++) {
					for (int k = 0; k < communityList.size(); k++) {
						if (j == k) {
							matrix[j][k] = 1;
						} else {
							Set<Node> intersaction = new HashSet<Node>();
							for (Node node : communityList.get(j)) {
								intersaction.add(node);
							}

							intersaction.retainAll(communityList.get(k));

							matrix[j][k] = 2.0
									* intersaction.size()
									/ (communityList.get(k).size() + communityList
											.get(j).size());
						}
					}
				}

				for (int j = 0; j < communityList.size(); j++) {
					for (int k = 0; k < communityList.size(); k++) {
						System.out.print(matrix[j][k] + ",");
					}
					System.out.println();

				}

				transaction.success();
			}

		}
	}

}
