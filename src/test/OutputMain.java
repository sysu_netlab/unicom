package test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import magic.Community;
import magic.MyLabel;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.helpers.collection.IteratorUtil;

import magic.Main;

public class OutputMain {
	public static ExecutorService pool = Executors.newCachedThreadPool();
	public final static Logger logger = Logger.getLogger("detection");

	public static void main(String[] args) {
		Main.DB_PATH = args[0];

		logger.setLevel(Level.INFO);

		try {
			Main.db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(
					Main.DB_PATH).newGraphDatabase();
			logger.info("Start database at: " + args[0]);
			Long start = System.currentTimeMillis();

			try (Transaction transaction = Main.db.beginTx();) {
				for (Node communityNode : IteratorUtil.asSet(Main.db
						.findNodes(MyLabel.COMMUNITY))) {
					Community community = new Community(communityNode);
					community.outputInfo();
				}
				transaction.success();
			}

			logger.info("Total time: " + (System.currentTimeMillis() - start)
					/ 1000 + "s.");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
	}

}
