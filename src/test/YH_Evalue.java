package test;

import magic.MyLabel;

import java.util.logging.Logger;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

public class YH_Evalue {

	public static String DB_PATH;
	public static GraphDatabaseService db;

	private final static Logger logger = Logger.getLogger("evalue");

	public static void startup() {
		try {
			db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(DB_PATH)
					.newGraphDatabase();

			logger.info("Start database \n");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			registerShutdownHook(db);
		}
	}

	private static void registerShutdownHook(final GraphDatabaseService graphDb) {
		System.out.println("Add shudown hook");
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDb.shutdown();
			}
		});
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		DB_PATH = args[0];
		
		startup();

		int size = 4;

		Long[] nodeId = { 45786L, 19949L, 29642L, 38447L };
		String[] name = { "Wang Fei", "Xiao Fang", "Wen Yuan", "Wen Jie" };

		try (Transaction transaction = db.beginTx();) {

			for (int i = 0; i < size; i++) {

				int numOfRaw = 0;
				int numOfRec = 0;

				Node node = db.getNodeById(nodeId[i]);

				for (Relationship rela : node.getRelationships()) {

					Node com = rela.getOtherNode(node);

					if (com.hasLabel(MyLabel.COMMUNITY)) {
						numOfRaw++;
					}

					if (com.hasLabel(MyLabel.RECOMMEND)) {
						numOfRec++;
					}
				}

				logger.info(name[i] + " has " + numOfRaw + " coms and recomend "
						+ numOfRec + " coms to her/him \n");
			}

			transaction.success();
		}

	}

}
